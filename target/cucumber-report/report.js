$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/Features/NewClientWorkflow.feature");
formatter.feature({
  "name": "NewClientWorkflow",
  "description": "  As a user\n  I want to be able to add new clients in the system\n  So that i can add accounting data for that client",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@x"
    }
  ]
});
formatter.scenario({
  "name": "Edit Customer",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@x"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on landing page",
  "keyword": "Given "
});
formatter.match({
  "location": "ClientSteps.landingPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the username as mngr201861",
  "keyword": "When "
});
formatter.match({
  "location": "ClientSteps.i_enter_the_username_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the password as tYqavys",
  "keyword": "And "
});
formatter.match({
  "location": "ClientSteps.i_enter_the_password_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the login button",
  "keyword": "And "
});
formatter.match({
  "location": "ClientSteps.i_click_the_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on Edit Customer link",
  "keyword": "And "
});
formatter.match({
  "location": "ClientSteps.i_click_on_Edit_Customer_link()"
});
formatter.result({
  "error_message": "org.openqa.selenium.UnhandledAlertException: unexpected alert open: {Alert text : User or Password is not valid}\n  (Session info: chrome\u003d76.0.3809.132): User or Password is not valid\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027dev-ws-10\u0027, ip: \u002710.64.5.110\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00275.2.13-arch1-1-ARCH\u0027, java.version: \u00271.8.0_222\u0027\nDriver info: org.openqa.selenium.remote.RemoteWebDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 76.0.3809.132, chrome: {chromedriverVersion: 76.0.3809.126 (d80a294506b4..., userDataDir: /tmp/.com.google.Chrome.yRCLsZ}, goog:chromeOptions: {debuggerAddress: localhost:36461}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webdriver.remote.sessionid: 11c523c2bf0687480508bde65bf...}\nSession ID: 11c523c2bf0687480508bde65bf7adb5\n*** Element info: {Using\u003dxpath, value\u003d//a[contains(text(),\u0027Edit Customer\u0027)]}\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:314)\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$000(ExpectedConditions.java:43)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:300)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:297)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:670)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:666)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:249)\n\tat Helpers.DriverHelper.ClickItem(DriverHelper.java:24)\n\tat Pages.LandingPage.ClickEditCustomer(LandingPage.java:73)\n\tat StepDefs.ClientSteps.i_click_on_Edit_Customer_link(ClientSteps.java:68)\n\tat ✽.I click on Edit Customer link(file:src/test/java/Features/NewClientWorkflow.feature:12)\n",
  "status": "failed"
});
formatter.step({
  "name": "I Enter customer ID as dd",
  "keyword": "When "
});
formatter.match({
  "location": "ClientSteps.i_Enter_customer_ID_as_dd(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/Features/Test2.feature");
formatter.feature({
  "name": "Test2",
  "description": "  As a user\n  I want to be able to add new clients in the system\n  So that i can add accounting data for that client",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@x"
    }
  ]
});
formatter.scenario({
  "name": "New Customer2",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@smoke"
    },
    {
      "name": "@x"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am on landing page",
  "keyword": "Given "
});
formatter.match({
  "location": "ClientSteps.landingPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the username as mngr201861",
  "keyword": "When "
});
formatter.match({
  "location": "ClientSteps.i_enter_the_username_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter the password as tYqavys",
  "keyword": "And "
});
formatter.match({
  "location": "ClientSteps.i_enter_the_password_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the login button",
  "keyword": "And "
});
formatter.match({
  "location": "ClientSteps.i_click_the_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on New Customer link",
  "keyword": "Given "
});
formatter.match({
  "location": "ClientSteps.i_click_on_New_Customer_link()"
});
formatter.result({
  "error_message": "org.openqa.selenium.UnhandledAlertException: unexpected alert open: {Alert text : User or Password is not valid}\n  (Session info: chrome\u003d76.0.3809.132): User or Password is not valid\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027dev-ws-10\u0027, ip: \u002710.64.5.110\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00275.2.13-arch1-1-ARCH\u0027, java.version: \u00271.8.0_222\u0027\nDriver info: org.openqa.selenium.remote.RemoteWebDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 76.0.3809.132, chrome: {chromedriverVersion: 76.0.3809.126 (d80a294506b4..., userDataDir: /tmp/.com.google.Chrome.8EkCe6}, goog:chromeOptions: {debuggerAddress: localhost:34433}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webdriver.remote.sessionid: a20f304af9055a0daccac3be9f6...}\nSession ID: a20f304af9055a0daccac3be9f6c636a\n*** Element info: {Using\u003dxpath, value\u003d//a[contains(text(),\u0027New Customer\u0027)]}\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:314)\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$000(ExpectedConditions.java:43)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:300)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:297)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:670)\n\tat org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:666)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:249)\n\tat Helpers.DriverHelper.ClickItem(DriverHelper.java:24)\n\tat Pages.LandingPage.ClickNewCustLink(LandingPage.java:63)\n\tat StepDefs.ClientSteps.i_click_on_New_Customer_link(ClientSteps.java:56)\n\tat ✽.I click on New Customer link(file:src/test/java/Features/Test2.feature:21)\n",
  "status": "failed"
});
formatter.step({
  "name": "I enter customer name as sul",
  "keyword": "When "
});
formatter.match({
  "location": "ClientSteps.i_enter_customer_name_as_sul(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});