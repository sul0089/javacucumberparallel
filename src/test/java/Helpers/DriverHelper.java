package Helpers;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DriverHelper {

    public static void WaitForItem(WebDriver driver,WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void WaitForItem(WebDriver driver,WebElement element,int waitTime){
        WebDriverWait wait = new WebDriverWait(driver, waitTime);
        wait.until(ExpectedConditions.stalenessOf(element));
    }

    public static void ClickItem(WebDriver driver,WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public static void VerifyNavigatedToPage(WebDriver driver, String url){
        WebDriverWait wait = new WebDriverWait(driver, 60);
       Assert.assertTrue(wait.until(ExpectedConditions.urlContains(url)));
    }

    public static void EnterText(WebDriver driver,WebElement element,String textToEnter){
        WaitForItem(driver,element);
        element.sendKeys(textToEnter);
    }
}

