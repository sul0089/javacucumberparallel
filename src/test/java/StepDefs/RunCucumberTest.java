package StepDefs;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features/", plugin = { "pretty","html:target/cucumber-report"})
public class RunCucumberTest {
}