package StepDefs;

import Setup.PropertyReader;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.Assert;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {
    private WebDriver driver;


    @Before
    public void initialize() throws MalformedURLException {
        if (driver == null)
            createNewDriverInstance();

    }

    private void createNewDriverInstance() throws MalformedURLException {

        PropertyReader propReader= new PropertyReader();
        String browser=propReader.readProperty("browser");
        String driverlocation=propReader.readProperty("driverlocation");
        DesiredCapabilities dc;
        RemoteWebDriver remoteDriver;

        switch (browser.toLowerCase()){
            case "chrome":
                System.setProperty("webdriver.chrome.driver",driverlocation+"/chromedriver.exe");
                driver= new ChromeDriver();
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver",driverlocation+"/geckodriver.exe");
                driver= new FirefoxDriver();
                break;

            case "remote chrome":
                dc=DesiredCapabilities.chrome();
                    remoteDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc);
                driver=remoteDriver;
                break;
            case "remote ff":
                dc=DesiredCapabilities.firefox();
                    remoteDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc);
                driver=remoteDriver;
                break;
            default:
                Assert.fail("Could not read a valid browser type from the config file");
        }

        String baseUrl=propReader.readProperty("baseurl");
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }


    public WebDriver getDriver() {
        return driver;
    }


    @After
    public void destroyDriver() {
        driver.quit();
        driver = null;
    }
}