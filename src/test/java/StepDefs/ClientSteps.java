package StepDefs;
import Pages.LandingPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;


public class ClientSteps {

    DriverFactory driverFactory;
    WebDriver driver;
    LandingPage _landPage;

    public ClientSteps(DriverFactory driverFactory){
        this.driverFactory=driverFactory;
        driver=driverFactory.getDriver();
        _landPage=new LandingPage(driver);
    }



    @Given("I am on landing page")
    public void landingPage()  {
        _landPage=new LandingPage(driver);
        _landPage.NavLandingPage();
    }


    @When("I enter the username as ([^\"]*)$")
    public void i_enter_the_username_as(String uname) {
        _landPage.EnterUsername(uname);

    }

    @When("I enter the password as ([^\"]*)$")
    public void i_enter_the_password_as(String passw) {
        _landPage.EnterPassw(passw);

    }

    @When("I click the login button")
    public void i_click_the_login_button() {
        _landPage.ClickLoginBtn();

    }

    @Then("I should be navigated to managerhome page")
    public void i_should_be_navigated_to_managerhome_page() {

        driver.quit();
    }

    @Given("I click on New Customer link")
    public void i_click_on_New_Customer_link() {
        _landPage.ClickNewCustLink();

    }

    @When("I enter customer name as ([^\"]*)$")
    public void i_enter_customer_name_as_sul(String name) {
        _landPage.EnterCustName(name);

    }

    @Given("I click on Edit Customer link")
    public void i_click_on_Edit_Customer_link() {
        _landPage.ClickEditCustomer();
    }

    @When("I Enter customer ID as ([^\"]*)$")
    public void i_Enter_customer_ID_as_dd(String custId) {
        _landPage.EnterCustId(custId);
    }

    }
