package Pages;
import Helpers.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LandingPage {

    WebDriver driver;
    @FindBy(name ="uid")
    private WebElement uidInput;

    @FindBy(name ="password")
    private WebElement passwInput;

    @FindBy(name ="btnLogin")
    private WebElement loginBtn;

    @FindBy(xpath = "//a[contains(text(),'New Customer')]")
    private WebElement NewCustomer;

    @FindBy(xpath = "//a[contains(text(),'Edit Customer')]")
    private WebElement EditCustomer;

    @FindBy(name = "name")
    private WebElement CustomerName;

    @FindBy(name = "cusid")
    private WebElement CustomerId;


    public LandingPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }


    public void NavLandingPage() {

        DriverHelper.VerifyNavigatedToPage(driver,"demo.guru99.com");
    }


    public void EnterUsername(String uname) {

        DriverHelper.EnterText(driver,uidInput,uname);
    }

    public void EnterPassw(String passw)
    {
        DriverHelper.EnterText(driver,passwInput,passw);

    }

    public void ClickLoginBtn() {

        DriverHelper.ClickItem(driver,loginBtn);
    }

    public void ClickNewCustLink() {
        DriverHelper.ClickItem(driver,NewCustomer);


    }

    public void EnterCustName(String name) {
        DriverHelper.EnterText(driver,CustomerName,name);
    }

    public void ClickEditCustomer() {
        DriverHelper.ClickItem(driver,EditCustomer);

    }

    public void EnterCustId(String custId) {
        DriverHelper.EnterText(driver,CustomerId,custId);
    }
}
